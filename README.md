# Next suspense example

This is a [Next.js](https://nextjs.org/) project to showcase a simple case of using [Suspense](https://react.dev/reference/react/Suspense) to display a loading state while slow components are being streamed from the server.

It was written as an example for this article on my website: <https://clotet.dev/blog/suspense-nextjs-server-components>

## Getting Started

Run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```
