import { Skeleton } from "@/components/ui/skeleton";
import { setTimeout } from "timers/promises";

export default async function SlowComponent() {
  const timeToRender = Math.random() * 3000;
  await setTimeout(timeToRender);

  console.log(`Rendered after ${timeToRender.toFixed(0)}ms`);

  return (
    <div className="w-96 px-6 py-4 rounded-lg border-2 border-muted-foreground bg-muted">
      <h1 className="text-lg font-semibold mb-2">Slow component</h1>
      <p>
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eius obcaecati
        vel assumenda aliquid nihil voluptates vero, reiciendis aspernatur, amet
        delectus, sequi odit sapiente beatae fuga a inventore error saepe?
        Eveniet!
      </p>
    </div>
  );
}

export function SlowComponentSkeleton() {
  return (
    <div className="w-96 px-6 py-4 rounded-lg border-2 border-muted">
      <Skeleton className="w-36 h-6 mb-4" />
      <Skeleton className="w-full h-32" />
    </div>
  );
}
