import { Suspense } from "react";
import SlowComponent, { SlowComponentSkeleton } from "./slow-component";

export default function Home() {
  return (
    <main className="flex w-full p-10 justify-center flex-wrap gap-4">
      <Suspense
        fallback={[...Array(6)].map((_, i) => (
          <SlowComponentSkeleton key={i} />
        ))}
      >
        {[...Array(10)].map((_, i) => (
          <SlowComponent key={i} />
        ))}
      </Suspense>
    </main>
  );
}
